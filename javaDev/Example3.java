class Example3 {
	public static void main (String args[]) {
		int var;
		double x;
		var = 10;
		x = 10.0;
		System.out.println ("Begin value variable var: " + var);
		System.out.println ("Begin value variable x: " + x);
		System.out.println ();
		var = var / 4;
		x = x / 4;
		System.out.println ("Value variable var after division: " + var);
		System.out.println ("Value variable x after division: " + x);
	}
}