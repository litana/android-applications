package pro.litana.p008_viewelements;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView myTextView = (TextView) findViewById(R.id.mytext);
        myTextView.setText("Android it's easy");

        Button myBtn = (Button) findViewById(R.id.myBtn);
        myBtn.setText("first button");
        myBtn.setEnabled(false);

        CheckBox myCnb = (CheckBox) findViewById(R.id.myCnb);
        myCnb.setChecked(true);
    }
}
