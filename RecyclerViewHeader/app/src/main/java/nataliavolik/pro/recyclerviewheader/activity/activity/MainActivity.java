package nataliavolik.pro.recyclerviewheader.activity.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;


import nataliavolik.pro.recyclerviewheader.R;
import nataliavolik.pro.recyclerviewheader.activity.fragment.SampleGridFragment;
import nataliavolik.pro.recyclerviewheader.activity.fragment.SampleListFragment;
import nataliavolik.pro.recyclerviewheader.activity.fragment.SampleListReversedFragment;


public class MainActivity extends FragmentActivity {
    public static final int FRAGMENT_COUNT = 3;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViews();
    }

    private void setupViews() {
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(new RecyclerFragmentPagerAdapter(getSupportFragmentManager()));
    }

    private class RecyclerFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public RecyclerFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return SampleListFragment.newInstance();
                case 1:
                    return SampleGridFragment.newInstance();
                case 2:
                    return SampleListReversedFragment.newInstance();
                default:
                    return SampleListFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return FRAGMENT_COUNT;
        }
    }
}
