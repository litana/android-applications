package nataliavolik.pro.recyclerviewheader.activity.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nataliavolik.pro.recyclerviewheader.R;
import nataliavolik.pro.recyclerviewheader.activity.RecyclerViewHeader;
import nataliavolik.pro.recyclerviewheader.activity.adapter.ColorItemsAdapter;


public class SampleListFragment extends Fragment {

    private RecyclerViewHeader recyclerHeader;
    private RecyclerView recycler;

    public static SampleListFragment newInstance() {
        SampleListFragment fragment = new SampleListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sample, container, false);
        setupViews(view);
        return view;
    }

    private void setupViews(View view) {
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(new ColorItemsAdapter(getActivity(), 12));

        recyclerHeader = (RecyclerViewHeader) view.findViewById(R.id.header);
        recyclerHeader.attachTo(recycler);
    }

}