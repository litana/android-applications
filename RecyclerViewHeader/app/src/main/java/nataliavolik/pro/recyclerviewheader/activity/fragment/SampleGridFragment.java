package nataliavolik.pro.recyclerviewheader.activity.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nataliavolik.pro.recyclerviewheader.R;
import nataliavolik.pro.recyclerviewheader.activity.RecyclerViewHeader;
import nataliavolik.pro.recyclerviewheader.activity.adapter.ColorItemsAdapter;


public class SampleGridFragment extends Fragment {

    private RecyclerViewHeader recyclerHeader;
    private RecyclerView recycler;

    public static SampleGridFragment newInstance() {
        SampleGridFragment fragment = new SampleGridFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sample, container, false);
        setupViews(view);
        return view;
    }

    private void setupViews(View view) {
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(new ColorItemsAdapter(getActivity(), 21));

        recyclerHeader = (RecyclerViewHeader) view.findViewById(R.id.header);
        recyclerHeader.attachTo(recycler);
    }

}
