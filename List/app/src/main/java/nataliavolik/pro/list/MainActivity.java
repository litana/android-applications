package nataliavolik.pro.list;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

    String[] names = { "ИНН:6574882312", "ИНН:6658325802", "ИНН:6579082412",
            "ИНН:6174886780", "ИНН:6522592312", "ИНН:6327182312", "ИНН:6574854432",
            "ИНН:9994762312", "ИНН:5674882312", "ИНН:3357882312", "ИНН:6975823312", };

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customlist);

        // находим список
        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.my_list_item, names);

        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);

    }
}