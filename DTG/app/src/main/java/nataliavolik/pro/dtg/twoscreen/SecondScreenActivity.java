package nataliavolik.pro.dtg.twoscreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import nataliavolik.pro.dtg.R;

public class SecondScreenActivity extends AppCompatActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_screen);
    }
}
