package nataliavolik.pro.dtg.sixscreen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import nataliavolik.pro.dtg.R;

//public class SixScreenActivity extends AppCompatActivity {
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_six_screen);
//    }
//}

public class SixScreenActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_six_screen);

        ArrayList image_details = getListData();
        final ListView lv1 = (ListView) findViewById(R.id.list_sing);
        lv1.setAdapter(new CustomListAdapter(this, image_details));
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                NewsItem newsData = (NewsItem) o;
                Toast.makeText(SixScreenActivity.this, "Selected :" + " " + newsData, Toast.LENGTH_LONG).show();
            }
        });
    }

    private ArrayList getListData() {
        ArrayList<NewsItem> results = new ArrayList<NewsItem>();
        NewsItem newsData = new NewsItem();
        newsData.setHeadline("ФИО: Акимбеков Бекболот Мелисович ");
        newsData.setReporterName("Должность: руководитель");
        newsData.setDate("Дата окончания: 26 Июль 2016");
        results.add(newsData);

        newsData.setHeadline("ФИО: Карымшакова Асель Мелисовна ");
        newsData.setReporterName("Должность: бухгалтер");
        newsData.setDate("Дата окончания: 28 Августа 2016");
        results.add(newsData);

        // Add some more dummy data for testing
        return results;
    }
}
