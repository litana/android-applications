package nataliavolik.pro.dtg.sevenscreen.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


import java.util.ArrayList;

import nataliavolik.pro.dtg.R;

public class SocialFundFragment extends Fragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.social_fund, container, false);

        ArrayList<SocialFund> searchResults = GetSearchResults();

        final ListView lv1 = (ListView)view.findViewById(R.id.social_fund);
        lv1.setAdapter(new CustomBaseAdapterSocial(getActivity(), searchResults));
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = lv1.getItemAtPosition(position);
                SocialFund fullObject = (SocialFund)o;
                //Toast.makeText(ListViewBlogPost.this, "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

    private ArrayList<SocialFund> GetSearchResults(){
        ArrayList<SocialFund> results = new ArrayList<SocialFund>();

        SocialFund sr1 = new SocialFund();
        sr1.setName("Наименование отчета:");
        sr1.setPeriod("Период:");
        sr1.setDate("Дата отправки:");
        sr1.setStatus("Статус:");
        results.add(sr1);

        sr1 = new SocialFund();
        sr1.setName("Наименование отчета:");
        sr1.setPeriod("Период:");
        sr1.setDate("Дата отправки:");
        sr1.setStatus("Статус:");
        results.add(sr1);

        sr1 = new SocialFund();
        sr1.setName("Наименование отчета:");
        sr1.setPeriod("Период:");
        sr1.setDate("Дата отправки:");
        sr1.setStatus("Статус:");
        results.add(sr1);

        sr1 = new SocialFund();
        sr1.setName("Наименование отчета:");
        sr1.setPeriod("Период:");
        sr1.setDate("Дата отправки:");
        sr1.setStatus("Статус:");
        results.add(sr1);

        return results;
    }

}