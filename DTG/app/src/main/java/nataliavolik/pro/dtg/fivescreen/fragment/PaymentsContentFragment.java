package nataliavolik.pro.dtg.fivescreen.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
//import android.widget.Toast;

import java.util.ArrayList;

import nataliavolik.pro.dtg.R;


public class PaymentsContentFragment extends Fragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ArrayList image_details = getListData();
        View view = inflater.inflate(R.layout.payments_list, container, false);
        CustomListAdapterPayments adapter = new CustomListAdapterPayments(getActivity(),image_details);
        final ListView lview = (ListView)view.findViewById(R.id.custom_list);
        lview.setAdapter(adapter);
        lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o;
                o = lview.getItemAtPosition(position);
                NewsItemPayments newsData = (NewsItemPayments) o;
                //Toast.makeText(PaymentsContentFragment.this, "Selected :" + " " + newsData, Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

    private ArrayList getListData() {
        ArrayList<NewsItemPayments> results = new ArrayList<NewsItemPayments>();
        NewsItemPayments newsData = new NewsItemPayments();
        newsData.setHeadline("Услуга: Налоговая отчетность");
        newsData.setReporterName("Сумма: 75 сом");
        newsData.setDate("Май 26, 2016, 13:35");
        results.add(newsData);

        // Add some more dummy data for testing
        return results;
    }
}
