package nataliavolik.pro.dtg.fourscreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

import nataliavolik.pro.dtg.R;
import nataliavolik.pro.dtg.fourscreen.adapter.RecyclerViewAdapter;

public class FourScreenActivity extends AppCompatActivity {

    ArrayList<String> SubjectNames;
    RecyclerView recyclerview;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_four_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerview = (RecyclerView)findViewById(R.id.recyclerview1);

        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerview.setLayoutManager(RecyclerViewLayoutManager);

        AddItemsToRecyclerViewArrayList();

        RecyclerView.Adapter adapter = new RecyclerViewAdapter(SubjectNames);

        recyclerview.setAdapter(adapter);

    }

    public void AddItemsToRecyclerViewArrayList(){

        SubjectNames = new ArrayList<>();
        SubjectNames.add("Информация о взаиморасчетах");
        SubjectNames.add("Информация об ЭЦП");
        SubjectNames.add("Ваши отчеты");
        SubjectNames.add("Личные сообщения");
        SubjectNames.add("Новости");
        SubjectNames.add("К списку компаний");
    }

}
