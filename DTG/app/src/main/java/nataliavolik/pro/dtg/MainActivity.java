package nataliavolik.pro.dtg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import nataliavolik.pro.dtg.onescreen.FirstScreenActivity;
import nataliavolik.pro.dtg.twoscreen.SecondScreenActivity;
import nataliavolik.pro.dtg.threescreen.ThirdScreenActivity;
import nataliavolik.pro.dtg.fourscreen.FourScreenActivity;
import nataliavolik.pro.dtg.fivescreen.FiveScreenActivity;
import nataliavolik.pro.dtg.sixscreen.SixScreenActivity;
import nataliavolik.pro.dtg.sevenscreen.SevenScreenActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button partOneButton;
    private Button partTwoButton;
    private Button partThreeButton;
    private Button partFourButton;
    private Button partFiveButton;
    private Button partSixButton;
    private Button partSevenButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();

        partOneButton = (Button) findViewById(R.id.partOneButton);
        partTwoButton = (Button) findViewById(R.id.partTwoButton);
        partThreeButton = (Button) findViewById(R.id.partThreeButton);
        partFourButton = (Button) findViewById(R.id.partFourButton);
        partFiveButton = (Button) findViewById(R.id.partFiveButton);
        partSixButton = (Button) findViewById(R.id.partSixButton);
        partSevenButton = (Button) findViewById(R.id.partSevenButton);

        partOneButton.setOnClickListener(this);
        partTwoButton.setOnClickListener(this);
        partThreeButton.setOnClickListener(this);
        partFourButton.setOnClickListener(this);
        partFiveButton.setOnClickListener(this);
        partSixButton.setOnClickListener(this);
        partSevenButton.setOnClickListener(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.app_name));
    }

    @Override
    public void onClick(View v) {
        if(v.equals(partOneButton)) {
            startActivity(FirstScreenActivity.class);
        } else if(v.equals(partTwoButton)) {
            startActivity(SecondScreenActivity.class);
        } else if(v.equals(partThreeButton)){
            startActivity(ThirdScreenActivity.class);
        } else if(v.equals(partFourButton)){
            startActivity(FourScreenActivity.class);
        } else if(v.equals(partFiveButton)){
            startActivity(FiveScreenActivity.class);
        } else if(v.equals(partSixButton)){
            startActivity(SixScreenActivity.class);
        } else{
            startActivity(SevenScreenActivity.class);
        }
    }

    private void startActivity(Class<?> activityClass) {
        Intent myIntent = new Intent(this, activityClass);
        startActivity(myIntent);
    }
}
