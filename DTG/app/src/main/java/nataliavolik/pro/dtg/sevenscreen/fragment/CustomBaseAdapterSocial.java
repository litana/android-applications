package nataliavolik.pro.dtg.sevenscreen.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import nataliavolik.pro.dtg.R;

public class CustomBaseAdapterSocial extends BaseAdapter{
    private static ArrayList<SocialFund> searchArrayList;

    private LayoutInflater mInflater;

    public CustomBaseAdapterSocial(Context context, ArrayList<SocialFund> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.social_fund_listview, parent, false);
            holder = new ViewHolder();
            holder.txtName = (TextView) convertView.findViewById(R.id.name);
            holder.txtPeriod = (TextView) convertView.findViewById(R.id.period);
            holder.txtDate = (TextView) convertView.findViewById(R.id.date);
            holder.txtStatus = (TextView) convertView.findViewById(R.id.status);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtName.setText(searchArrayList.get(position).getName());
        holder.txtPeriod.setText(searchArrayList.get(position).getPeriod());
        holder.txtDate.setText(searchArrayList.get(position).getDate());
        holder.txtStatus.setText(searchArrayList.get(position).getStatus());

        return convertView;
    }

    static class ViewHolder {
        TextView txtName;
        TextView txtPeriod;
        TextView txtDate;
        TextView txtStatus;
    }
}
