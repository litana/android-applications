package nataliavolik.pro.dtg.onescreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import nataliavolik.pro.dtg.R;


public class FirstScreenActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);
    }
}
