package nataliavolik.pro.dtg.fivescreen.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import nataliavolik.pro.dtg.R;

//public class ReceiptsContentFragment extends Fragment {
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.receipts_list, container, false);
//    }
//}

public class ReceiptsContentFragment extends Fragment {
    private ArrayList<HashMap<String, String>> list;
    public static final String FIRST_COLUMN = "Column 1";
    public static final String SECOND_COLUMN = "Column 2";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.receipts_list, container, false);
        ListView lview = (ListView) view.findViewById(R.id.listview);
        populateList();
        listviewAdapterReceipts adapter = new listviewAdapterReceipts(getActivity(), list);
        lview.setAdapter(adapter);
        return view;
    }

    private void populateList() {

        list = new ArrayList<HashMap<String, String>>();

        HashMap<String, String> temp1 = new HashMap<String, String>();
        temp1.put(FIRST_COLUMN, "Мобильник");
        temp1.put(SECOND_COLUMN, "52 сом");
        list.add(temp1);

        HashMap<String, String> temp2 = new HashMap<String, String>();
        temp2.put(FIRST_COLUMN, "Qiwi");
        temp2.put(SECOND_COLUMN, "100 сом");
        list.add(temp2);
    }
}

