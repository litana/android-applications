package nataliavolik.pro.dtg.fivescreen.fragment;

//import android.content.Context;
//import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import nataliavolik.pro.dtg.R;

public class CustomListAdapterPayments extends BaseAdapter {
    private ArrayList<NewsItemPayments> listData;
    private LayoutInflater layoutInflater;
    //Activity activity;

    public CustomListAdapterPayments (FragmentActivity aContext, ArrayList<NewsItemPayments> listData) {
        this.listData = listData;
        //this.activity = activity;
        //layoutInflater = LayoutInflater.from(getContext());
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.payments_listview, parent,false);
            holder = new ViewHolder();
            holder.headlineView = (TextView) convertView.findViewById(R.id.title);
            holder.reporterNameView = (TextView) convertView.findViewById(R.id.reporter);
            holder.reportedDateView = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.headlineView.setText(listData.get(position).getHeadline());
        holder.reporterNameView.setText(" " + listData.get(position).getReporterName());
        holder.reportedDateView.setText(listData.get(position).getDate());
        return convertView;
    }

    static class ViewHolder {
        TextView headlineView;
        TextView reporterNameView;
        TextView reportedDateView;
    }
}